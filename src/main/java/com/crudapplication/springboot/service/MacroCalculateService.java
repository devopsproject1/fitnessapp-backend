package com.crudapplication.springboot.service;
import com.crudapplication.springboot.model.User;

public class MacroCalculateService extends User {

    private int bmr;
    private int amountOfProteines;
    private int amountOfCarbs;
    private int amountOfFats;
    private int percentageProteins = 40;
    private int percentageCarbs = 40;
    private int percentageFats = 20;
    private int carbEnergy;
    private int proteinEnergy;
    private int fatEnergy;

    public int calculateBmr(String gender, int age, int weigth, int length, double activity) {
       double calcBmr;

        if(gender.equals("man")) {
            calcBmr = 10 * weigth + 6.25 * length - 5 * age + 5;
        } else {
            calcBmr = 10 * weigth + 6.25 * length - 5 * age - 161;
        }
        this.bmr = (int) calcBmr;
        System.out.println(bmr);
        return bmr;
    }

    public void calculateMacroAmounts(int proteines, int carbs, int fats) {

        double fatsAmount = fats / 9;
        this.amountOfFats = (int) fatsAmount;

        double carbsAmount = carbs / 4;
        this.amountOfCarbs = (int) carbsAmount;

        double proteinsAmount = proteines / 4;
        this.amountOfProteines = (int) proteinsAmount;
    }

    public void calculateEnergy(int bmr) {
          double proteines = bmr * 0.4;
          this.proteinEnergy = (int) proteines;

          double carbs = bmr * 0.4;
          this.carbEnergy = (int) carbs;

          double fats = bmr * 0.2;
          this.fatEnergy = (int) fats;
    }

    public int getBmr() {
        return bmr;
    }

    public int getAmountOfProteines() {
        return amountOfProteines;
    }

    public void setAmountOfProteines(int amountOfProteines) {
        this.amountOfProteines = amountOfProteines;
    }

    public int getAmountOfCarbs() {
        return amountOfCarbs;
    }

    public void setAmountOfCarbs(int amountOfCarbs) {
        this.amountOfCarbs = amountOfCarbs;
    }

    public int getAmountOfFats() {
        return amountOfFats;
    }

    public void setAmountOfFats(int amountOfFats) {
        this.amountOfFats = amountOfFats;
    }

    public int getPercentageProteins() {
        return percentageProteins;
    }

    public void setPercentageProteins(int percentageProteins) {
        this.percentageProteins = percentageProteins;
    }

    public int getPercentageCarbs() {
        return percentageCarbs;
    }

    public void setPercentageCarbs(int percentageCarbs) {
        this.percentageCarbs = percentageCarbs;
    }

    public int getPercentageFats() {
        return percentageFats;
    }

    public void setPercentageFats(int percentageFats) {
        this.percentageFats = percentageFats;
    }

    public int getCarbEnergy() {
        return carbEnergy;
    }

    public int getProteinEnergy() {
        return proteinEnergy;
    }

    public int getFatEnergy() {
        return fatEnergy;
    }

}
