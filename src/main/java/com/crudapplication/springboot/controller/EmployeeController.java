package com.crudapplication.springboot.controller;
import com.crudapplication.springboot.model.User;
import com.crudapplication.springboot.service.MacroCalculateService;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;


@RestController
@CrossOrigin
@RequestMapping("/api/v1")
public class EmployeeController {

    @PostMapping("/tdee")
    public MacroCalculateService calculateTdee(@Valid @RequestBody User user) {

        System.out.println(user.getGender());
        System.out.println(user.getAge());
        System.out.println(user.getLength());
        System.out.println(user.getActivity());

        MacroCalculateService bmr = new MacroCalculateService();
        int calcBmr = bmr.calculateBmr(user.getGender(), user.getAge(), user.getWeight(), user.getLength(), user.getActivity());
        bmr.calculateEnergy(calcBmr);
        bmr.calculateMacroAmounts(bmr.getProteinEnergy(), bmr.getCarbEnergy(), bmr.getFatEnergy());
        return bmr;
    }
}
